" Edit factories
if filereadable("spec/factories.rb")
  command! Rfactories :e spec/factories.rb
  command! RTfactories :tabe spec/factories.rb
  command! RSfactories :split spec/factories.rb
else
  autocmd User Rails Rnavcommand factory spec/factories -glob=* -suffix=.rb -default=model()
endif

" Leader shortcuts for Rails commands
autocmd User Rails map <Leader>m :Rmodel 
autocmd User Rails map <Leader>v :Rview 
autocmd User Rails map <Leader>c :Rcontroller 
autocmd User Rails map <Leader>u :Runittest 
autocmd User Rails map <Leader>f :Rfunctionaltest 
autocmd User Rails map <Leader>i :Rintegrationtest 
autocmd User Rails map <Leader>h :Rhelper 
autocmd User Rails map <Leader>j :Rjavascript 

autocmd User Rails map <Leader>tm :RTmodel 
autocmd User Rails map <Leader>tv :RTview 
autocmd User Rails map <Leader>tc :RTcontroller 
autocmd User Rails map <Leader>tu :RTunittest 
autocmd User Rails map <Leader>tf :RTfunctionaltest 
autocmd User Rails map <Leader>ti :RTintegrationtest 
autocmd User Rails map <Leader>th :RThelper 
autocmd User Rails map <Leader>tj :RTjavascript 

autocmd User Rails map <Leader>sm :RVmodel 
autocmd User Rails map <Leader>sv :RVview 
autocmd User Rails map <Leader>sc :RVcontroller 
autocmd User Rails map <Leader>su :RVunittest 
autocmd User Rails map <Leader>sf :RVfunctionaltest 
autocmd User Rails map <Leader>si :RVintegrationtest 
autocmd User Rails map <Leader>sh :RVhelper 
autocmd User Rails map <Leader>sj :RVjavascript 

" Rconfig for config navigation
autocmd User Rails Rnavcommand config config -glob=**/* -suffix=.rb -default=routes
