filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Let Vundle manage Vundle
Bundle 'gmarik/vundle'

" ------------------------------
" General
" ------------------------------

Bundle 'kana/vim-textobj-user'
Bundle 'Lokaltog/vim-powerline'
Bundle 'mileszs/ack.vim'
Bundle 'tpope/vim-endwise'
Bundle 'tpope/vim-eunuch'
Bundle 'tpope/vim-repeat'
Bundle 'tpope/vim-speeddating'
Bundle 'tpope/vim-surround'
Bundle 'tpope/vim-unimpaired'

Bundle 'kien/ctrlp.vim'
    let g:ctrlp_working_path_mode = 2
    let g:ctrlp_max_depth = 15
    let g:ctrlp_max_files = 5000
    let g:ctrlp_dotfiles = 0
    let g:ctrlp_custom_ignore = '\.git$\|\.hg$\|\.svn$'
    map <Leader>b :CtrlPBuffer<CR>

    " Start CtrlP scoped to the current file's directory
    map <Leader>e :CtrlP %%<cr>
    map <Leader>te :CtrlP %%<cr>

Bundle 'Tabular'
    map <Leader>a= :Tabularize /=<CR>
    map <Leader>a> :Tabularize /=><CR>
    map <Leader>a: :Tabularize properties<CR>
    map <Leader>at :Tabularize table<CR>

    vmap <Leader>a= :Tabularize /=<CR>gv
    vmap <Leader>a> :Tabularize /=><CR>gv
    vmap <Leader>a: :Tabularize properties<CR>gv
    vmap <Leader>at :Tabularize table<CR>

    imap <Leader>a= <Esc>:Tabularize /=<CR>a
    imap <Leader>a> <Esc>:Tabularize /=><CR>a
    imap <Leader>a: <Esc>:Tabularize properties<CR>a
    imap <Leader>at :Tabularize table<CR>

Bundle 'tComment'
    nmap <Leader>cc <C-_><C-_>
    vmap <Leader>cc <C-_><C-_>

Bundle 'scrooloose/nerdtree'
    let g:NERDTreeIgnore      = ['\.rbc$', '\~$', '.DS_Store$']
    let g:NERDTreeChDirMode   = 2
    let g:NERDTreeQuitOnOpen  = 1
    let g:NERDTreeMinimalUI   = 1
    let g:NERDTreeDirArrows   = 0
    let g:NERDTreeHijackNetrw = 1
    map <Leader>/ :NERDTreeFind<CR>

    autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

    " Close all open buffers on entering a window if the only
    " buffer that's left is the NERDTree buffer
    function! s:CloseIfOnlyNerdTreeLeft()
      if exists("t:NERDTreeBufName")
        if bufwinnr(t:NERDTreeBufName) != -1
          if winnr("$") == 1
            q
          endif
        endif
      endif
    endfunction

" ------------------------------
" CoffeeScript / JavaScript
" ------------------------------

Bundle 'kchmck/vim-coffee-script'
" Bundle 'pangloss/vim-javascript'

" ------------------------------
" Git
" ------------------------------

Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-git'

" ------------------------------
" Ruby
" ------------------------------

Bundle 'tpope/vim-cucumber'
Bundle 'tpope/vim-haml'
Bundle 'tpope/vim-rails'
Bundle 'tpope/vim-rake'
Bundle 'vim-ruby/vim-ruby'

" ------------------------------
" Text objects
" ------------------------------

Bundle 'argtextobj.vim'
Bundle 'michaeljsmith/vim-indent-object'
Bundle 'nelstrom/vim-textobj-rubyblock'

" ------------------------------
" Misc
" ------------------------------

Bundle 'nginx.vim'
Bundle 'juvenn/mustache.vim'
Bundle 'tpope/vim-markdown'

" ------------------------------
" Experimental
" ------------------------------

Bundle 'chriskempson/vim-tomorrow-theme'

Bundle 'Gundo'
    nnoremap <Leader>u :GundoToggle<CR>

Bundle 'kien/rainbow_parentheses.vim'
    autocmd VimEnter * RainbowParenthesesToggle
    autocmd Syntax * RainbowParenthesesLoadRound
    autocmd Syntax * RainbowParenthesesLoadSquare
    autocmd Syntax * RainbowParenthesesLoadBraces

Bundle 'ZoomWin'
    map <Leader>z :ZoomWin<CR>
    imap <Leader>z <Esc>:ZoomWin<CR>a

Bundle 'applescript.vim'

Bundle 'jc00ke/vim-tomdoc'

Bundle 'rson/vim-conque'
    let g:ConqueTerm_ReadUnfocused = 1
    let g:ConqueTerm_InsertOnEnter = 0
Bundle 'skwp/vim-ruby-conque'
    let g:ruby_conque_rspec_command = 'bundle exec rspec'

    " Cmd-Shift-R for RSpec
    nmap <silent> <D-R> :call RunRspecCurrentFileConque()<CR>
    " Cmd-Shift-L for RSpec Current Line
    nmap <silent> <D-L> :call RunRspecCurrentLineConque()<CR>

    nmap <silent> <Leader>t :call RunLastConqueCommand()<CR>

Bundle 'AndrewRadev/switch.vim'
    nnoremap - :Switch<cr>

filetype plugin indent on
