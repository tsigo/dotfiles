require 'rake'

task :default => :install

desc "install the dot files into user's home directory"
task :install do
  replace_all = false
  dot_files   = File.dirname(__FILE__)
  files       = [
    'bash/bash_profile',
    'bash/bashrc',
    'git/gitignore',
    'js',
    'misc/ackrc',
    'misc/inputrc',
    'misc/tmux.conf',
    'ruby/gemrc',
    'ruby/irbrc',
    'ruby/rspec',
    'vim',
    'vim/gvimrc',
    'vim/vimrc',
    'zsh',
    'zsh/zshenv',
    'zsh/zshrc',
  ]

  files = Hash[files.zip(Array.new(files.size, "~/"))]
  # files["ruby/global.gems"] = "~/.rvm/gemsets/"

  files.each do |file, destination|
    file_name        = file.split(/\//).last
    source_file      = File.join(dot_files, file)
    destination_file = File.expand_path(File.join(destination, ".#{file_name}"))

    if File.exist?(destination_file) || File.symlink?(destination_file)
      if replace_all
        replace_file(destination_file, source_file)
      else
        print "overwrite #{destination_file}? [ynaq] "
        case $stdin.gets.chomp.downcase
        when 'a'
          replace_all = true
          replace_file(destination_file, source_file)
        when 'y'
          replace_file(destination_file, source_file)
        when 'q'
          exit
        else
          puts "skipping #{destination_file}"
        end
      end
    else
      link_file(destination_file, source_file)
    end
  end

  # Copy gitconfig instead of symlinking it so we can modify it with our github tokens
  # There is no confirmation for this, so... tough titties if something's destroyed
  destination_file = File.expand_path("~/.gitconfig")
  system "rm #{destination_file}" if File.exists?(destination_file)
  puts "#{destination_file} => git/gitconfig"
  system "cp git/gitconfig ~/.gitconfig"
  system "cat git/github_tokens >> ~/.gitconfig" if File.exists?('git/github_tokens')
end

def replace_file(old_file, new_file)
  system %Q{rm "#{old_file}"}
  link_file(old_file, new_file)
end

def link_file(old_file, new_file)
  puts "#{old_file} => #{new_file}"
  system %Q{ln -fs "#{new_file}" "#{old_file}"}
end
