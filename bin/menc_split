#!/usr/bin/env ruby

require 'rubygems'
require 'trollop'
require 'chronic'

class Splitter
  def initialize(filename)
    @input = filename
  end

  def filename(sequence = false)
    if sequence
      @input.gsub(/(.*)(\.\w+)$/, '\1-part' + self.next_sequence + '\2')
    else
      @input.gsub(/(.*)(\.\w+)$/, '\1-split\2')
    end
  end

  def arguments(opts = {})
    args = [
      '-oac copy',
      '-ovc copy'
    ]

    if opts[:start]
      args << "-ss #{opts[:start]}"
    end

    if opts[:end]
      if opts[:start]
        args << "-endpos #{Chronic.parse(opts[:end]) - Chronic.parse(opts[:start])}"
      else
        args << "-endpos #{opts[:end]}"
      end
    end

    # Quoted input filename
    args << %["#{@input}"]

    # Quoted output filename, in same directory as input, with optional sequence number
    args << %[-o "#{File.dirname(@input)}/#{self.filename(opts[:sequence])}"]

    # Don't try to auto-correct A/V sync issues
    args << '-mc 0'
    args << '-noskip'

    args.join(' ')
  end

  protected

  def next_sequence
    # Get the list of files in this file's directory
    entries = Dir.entries(File.dirname(File.expand_path(@input)))

    # Get only those matching a sequenced file
    entries.select! { |e| e =~ /.*-part(\d+).*/i }

    # Remove everything but the part number from each entry
    entries.map!    { |e| e.gsub(/.*-part(\d+).*/i, '\1').to_i }

    # Sort the entries then get the last one then add one to get the next in the sequence
    (entries.length > 0) ? (entries.sort[-1] + 1).to_s : '1'
  end
end

if ARGV.delete('--test')
  require 'minitest/autorun'
  require 'mocha'

  class SplitterTest < MiniTest::Unit::TestCase
    def setup
      @splitter = Splitter.new('movie.avi')
    end

    def test_filename_without_sequence
      assert_equal('movie-split.avi', @splitter.filename)
    end

    def test_filename_with_sequence
      Dir.expects(:entries).once.returns(['movie.avi', 'movie-part1.avi', 'movie-part2.avi'])
      assert_equal('movie-part3.avi', @splitter.filename(true))

      Dir.expects(:entries).once.returns(['movie.avi'])
      assert_equal('movie-part1.avi', @splitter.filename(true))
    end

    def test_arguments
      assert_match('-oac copy',               @splitter.arguments)
      assert_match('-ovc copy',               @splitter.arguments)
      assert_match('-ss 00:01:30 -endpos 30', @splitter.arguments(start: '00:01:30', :end => '00:02:00'))
      assert_match('-endpos 00:02:00',        @splitter.arguments(:end => '00:02:00'))
      assert_match('"movie.avi"',             @splitter.arguments)
      assert_match('-o "./movie-part1.avi"',  @splitter.arguments(sequence: true))
      assert_match('-mc 0',                   @splitter.arguments)
      assert_match('-noskip',                 @splitter.arguments)
    end
  end
else
  opts = Trollop::options do
    version "menc_split 1.1.0 (c) 2011 Robert Speicher"
    banner <<-EOS
  menc_split provides a simple interface to splitting video files using mencoder (mplayer)

  Usage:
      menc_split [options] filename
  EOS

    opt :start,    "Start time in hh:mm:ss",                :type => String
    opt :end,      "End time in hh:mm:ss",                  :type => String
    opt :sequence, "Automatically sequence resulting file", :default => false
    opt :pretend,  "Don't actually call mencoder",          :default => false
  end

  Trollop::die "provide a start and/or end time" unless opts[:start] || opts[:end]
  Trollop::die "provide an input file" if ARGV.empty? || !File.exist?(ARGV[0])

  @s = Splitter.new(ARGV[0])

  call = "mencoder " + @s.arguments(opts)
  puts call
  exec call unless opts[:pretend]
end
