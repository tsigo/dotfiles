// Add a confirmation before leaving pages with an active video

$(function() {
  window.onbeforeunload = function() {
    if ($('object[id^="vp_"],object[id^="fp_"]').length > 0) {
      return "Are you sure you want to leave this page?"
    }
  }
})
