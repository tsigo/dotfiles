(function() {

  $(function() {
    var container, hide, isHidden, processItems, setupGlobalToggle, setupToggleButtons, show;
    container = 'div.feed-item-container';
    isHidden = function(elem) {
      return localStorage.getItem("hidden:" + ($(elem).data('id'))) != null;
    };
    hide = function(elem) {
      $(elem).data('hidden', true);
      return localStorage.setItem("hidden:" + ($(elem).data('id')), true);
    };
    show = function(elem) {
      $(elem).data('hidden', false);
      return localStorage.removeItem("hidden:" + ($(elem).data('id')));
    };
    $('body').append('<style type="text/css" id="hide-feed-item-css"></style>');
    $('#hide-feed-item-css').text('.hidden {\n  display: none;\n}\ndiv.feed-header-subscribe label {\n  margin-left: 15px;\n}\nspan.hide-feed-item, span.show-feed-item {\n  position: absolute;\n  top: 15px;\n  right: 2px;\n  color: #BEBEBE;\n  border: 1px solid #BEBEBE;\n  padding: 3px 4px;\n  font-weight: bold;\n  font-size: 70%;\n  line-height: 6px;\n}\nspan.show-feed-item {\n  color: green;\n  border-color: green;\n}\nspan.hide-feed-item:hover, span.show-feed-item:hover {\n  color: black;\n  border-color: black;\n  cursor: pointer;\n}');
    setupGlobalToggle = function() {
      var form_elements, hidden_toggle, subscribe_toggle;
      subscribe_toggle = $('div.feed-header-subscribe label');
      hidden_toggle = subscribe_toggle.clone();
      form_elements = $('span', subscribe_toggle).first().clone();
      $(hidden_toggle).text('Show hidden items');
      $(hidden_toggle).prepend(form_elements);
      $('span', hidden_toggle).removeClass('checked');
      $('input', hidden_toggle).attr('name', 'show_hidden').attr('id', 'show_hidden').removeAttr('checked').removeClass('feed-filter');
      $(hidden_toggle).click(function() {
        return processItems();
      });
      return $('div.feed-header-subscribe').append(hidden_toggle);
    };
    setupToggleButtons = function(elem) {
      if (isHidden(elem)) {
        $('.hide-feed-item', elem).addClass('hidden');
        return $('.show-feed-item', elem).removeClass('hidden');
      } else {
        $('.hide-feed-item', elem).removeClass('hidden');
        return $('.show-feed-item', elem).addClass('hidden');
      }
    };
    processItems = function() {
      return $(container).each(function() {
        var id,
          _this = this;
        id = $('a[href^="/watch"]', this).first().attr('href').replace(/.*v=([^&]+).*/, '$1');
        $(this).data('id', id);
        $(this).data('hidden', isHidden(this));
        if ($('#show_hidden:checked').length === 1) {
          $(this).removeClass('hidden');
        } else {
          if (isHidden(this)) {
            $(this).addClass('hidden');
          } else {
            $(this).removeClass('hidden');
          }
        }
        $('img[data-thumb][alt*="Thumbnail"]', this).attr('src', function() {
          return $(this).data('thumb');
        });
        if ($('.hide-feed-item, .show-feed-item', this).length === 0) {
          $('.feed-item-main', this).append('<span class="hide-feed-item" title="Hide this item">X</span>');
          $('.feed-item-main', this).append('<span class="show-feed-item" title="Show this item">&#x2713;</span>');
        }
        setupToggleButtons(this);
        $('.hide-feed-item', this).click(function() {
          hide(_this);
          setupToggleButtons(_this);
          return processItems();
        });
        return $('.show-feed-item', this).click(function() {
          show(_this);
          setupToggleButtons(_this);
          return processItems();
        });
      });
    };
    setupGlobalToggle();
    processItems();
    return $('button.feed-load-more').live('click', function() {
      return setTimeout(processItems, 500);
    });
  });

}).call(this);
