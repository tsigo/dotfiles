// Automatically loads the next page of posts when we hit the bottom of the current page

var content_id  = 'div#posts'
var waypoint_id = 'div.pagenav:last'
var next_id     = 'td a[title^="Next Page"]'

function successCallback(d) {
  d = $(d)
  $(content_id).append(d.find(content_id).html())
  $(waypoint_id).replaceWith(d.find(waypoint_id))

  removeQuotedImages()
  quoteHighlight()
}

if ($(content_id).length > 0 && $(waypoint_id).find(next_id).length > 0) {
  $(window).bind('scroll', function() {
    paginationWaypoint(waypoint_id, next_id, waypointCallback)
  })
}

// Highlight people quoting my posts

function quoteHighlight() {
  $('#posts strong:contains("tsigo")').each(function() {
    $(this).parent().css('font-size', '2em')
    $(this).parent().css('color', 'red')
  })
}

// Change multiple quoted images into links

function removeQuotedImages() {
  $('.quotearea').each(function() {
    if ($(this).find('img').length > 1) {
      $(this).find('img').each(function() {
        var src = $(this).attr('src')
        $(this).parent().append('<a href="' + src + '">' + src + '</a><br/>')
        $(this).remove()
      })
    }
  })
}

$(function() {
  removeQuotedImages()
  quoteHighlight()
})
