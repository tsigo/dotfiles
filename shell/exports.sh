EDITOR="vim"

export EDITOR="$EDITOR"
export VISUAL="$EDITOR"
export GEM_OPEN_EDITOR="$EDITOR"
export GIT_EDITOR="vim -f" # http://is.gd/hGrsF

export IRBRC="$HOME/.irbrc"
export JEWELER_OPTS="--rspec --gemcutter --rubyforge --reek --roodi"

#export TERM=xterm-256color
export GREP_OPTIONS='--color=auto' 
export GREP_COLOR='1;32'
export CLICOLOR=1

export LESS=-RFX

export HISTSIZE=1000000

export REPORTTIME=2
export TIMEFMT="%*Es total, %U user, %S system, %P cpu"

export DISABLE_AUTO_UPDATE="true"
