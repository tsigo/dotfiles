# Allow brackets to work for rake tasks
alias rake='noglob rake'

alias -g G='| grep'
alias -g M='| less'
alias -g L='| wc -l'
alias -g ONE="| awk '{ print \$1}'"
alias -g WRF="|while read file; do"

alias psname="ps aux | grep -i"

alias e="$EDITOR"
alias et="$EDITOR ."

# git
alias g='git'
alias gb='git branch'
alias gc='git commit -v'
alias gca='gc --amend'
alias gco='git checkout'
alias gd='git diff'
alias gp='git push'
alias gs='git status'
alias gx='gitx'

# ls
alias ls="ls -F"
alias l="ls -lAh"
alias ll="ls -l"
alias la='ls -A'

# ruby
alias b='bundle'
alias be='bundle exec'
alias cuc='cucumber'
alias migrate='rake db:migrate db:test:prepare'
alias cover='rake spec:rcov && open coverage/index.html'
alias docapp='rake doc:app && open doc/app/index.html'
alias devlog='tail -f log/development.log'
alias gemd='gem query -d -n'

# tmux
alias t="tmux"
alias ta="tmux attach"
alias tls="tmux list-sessions"
alias tk="tmux kill-server"

# servers
alias glutt='ssh tsigo@gluttony'
alias tsigo='ssh tsigo@tsigo.com'

# useful command to find what you should be aliasing:
alias profileme="history | awk '{print \$2}' | awk 'BEGIN{FS=\"|\"}{print \$1}' | sort | uniq -c | sort -n | tail -n 20 | sort -nr"

alias youtube-mp3="youtube-dl -t --no-mtime --extract-audio --audio-format mp3 --audio-quality 192"
