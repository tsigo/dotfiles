# tsigo/dotfiles

My dotfiles. Obviously.

## Thanks

Some of this was inspired by these people, other parts were copied entirely.

* [Bodaniel Jeanes](https://github.com/bjeanes/dot-files)
* [Joe Ferris](https://github.com/jferris)
* [Joshua Clayton](https://github.com/joshuaclayton)
* [Tim Pope](https://github.com/tpope)
